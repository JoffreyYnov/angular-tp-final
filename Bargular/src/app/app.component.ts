import { Component } from '@angular/core';

import { UserService } from './user/user.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})

export class AppComponent {

	title = 'Bargular';

	constructor(private userService: UserService, private router: Router) { }

	isConnected() {
		if (this.userService.LoggedUser.username == null) {
			return (false);
		}
		return (true);
	}

	isNotConnected() {
		if (this.userService.LoggedUser.username == null) {
			return (true);
		}
		return (false);
	}

	appLogOut() {
		this.userService.logOut();
	}
}
