import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfileGuardGuard } from './profile-guard.guard';
import { PlanningComponent } from './planning/planning.component';

import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


const routes: Routes = [
	{
		path: '',
		component: HomeComponent
	},
	{
		path: 'home',
		redirectTo: '',
		pathMatch: 'full'
	},
	{
		path: 'planning',
		component: PlanningComponent
	},
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'profile',
		canActivate: [ProfileGuardGuard],
		component: ProfileComponent
	},
	{
		path: '**',
		component: PageNotFoundComponent
	}
];

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		PageNotFoundComponent,
		HomeComponent,
		ProfileComponent,
		PlanningComponent,
		MainNavComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		HttpClientModule,
		RouterModule.forRoot(routes),
		BrowserAnimationsModule,
		LayoutModule,
		MatToolbarModule,
   	MatButtonModule,
   	MatSidenavModule,
   	MatIconModule,
   	MatListModule
	],
	exports: [
		RouterModule
	],
	providers: [],
	bootstrap: [AppComponent]
})

export class AppModule {}
