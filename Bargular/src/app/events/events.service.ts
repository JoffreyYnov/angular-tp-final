import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class EventsService {

	public serviceData = "./assets/events.json";

	constructor(private http: HttpClient) { }

	getEvents(): Observable<any> {
		return this.http.get(this.serviceData);
	}
}
