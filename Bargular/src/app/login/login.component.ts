import { Component, OnDestroy, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { Subscription } from 'rxjs/index';
import { UserService } from '../user/user.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit, OnDestroy {

	public Users;
	test = "test";

	login = {
		name : "",
		password : "",
		role : 0
	};

	isConnected = false;

	UserSubscription: Subscription;


	constructor(private userService: UserService, private router: Router) { }

	ngOnInit() {
		this.UserSubscription = this.userService.getUsers().subscribe((data) => {
			console.log(data);
			this.Users = data;
		});
	}

	ngOnDestroy() {
		if (this.UserSubscription) {
			this.UserSubscription.unsubscribe();
		}
	}


	public tryToLogIn() {
		console.log("trying to connect with : '" + this.login.name + "' + '" + this.login.password+ "'");

		// parse data.json for username
		for (var i = this.Users.length - 1; i >= 0; i--) {
			console.log("trying... " + this.Users[i].name);

			if (this.Users[i].name == this.login.name) {

				if (this.Users[i].password == this.login.password) {

					this.userService.LoggedUser.username = this.login.name;
					this.userService.LoggedUser.role = this.Users[i].role;
					console.log("Connected as : " + this.login.name);
					this.isConnected = true;
					this.router.navigate(['profile']);
					return (0);
				}
			}
		}
		console.log("Connection Failed !");
		return (1);
	}

	public testPost() {
		this.userService.userTestPost();
	}

}
