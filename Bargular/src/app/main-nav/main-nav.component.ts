import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserService } from '../user/user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, 
  	private userService: UserService, 
  	private router: Router) { }

	isConnected() {
		if (this.userService.LoggedUser.username == null) {
			return (false);
		}
		return (true);
	}

	isNotConnected() {
		if (this.userService.LoggedUser.username == null) {
			return (true);
		}
		return (false);
	}

	appLogOut() {
		this.userService.logOut();
	}
}
