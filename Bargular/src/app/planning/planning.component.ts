import { Component, OnInit } from '@angular/core';

import { UserService } from '../user/user.service';
import { EventsService } from '../events/events.service';
import { Subscription } from 'rxjs/index';


@Component({
  selector: 'app-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss']
})

export class PlanningComponent implements OnInit {

	private EventsSubscription: Subscription;

	public Events;

	constructor(private userService: UserService, private eventsService: EventsService) { }

	ngOnInit() {
		this.EventsSubscription = this.eventsService.getEvents().subscribe((data) => {
			console.log(data);
			this.Events = data;
		});
	}

}
