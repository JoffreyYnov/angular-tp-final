import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { UserService } from './user/user.service';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class ProfileGuardGuard implements CanActivate {

	constructor(private userService: UserService, 
		private router: Router) {}

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		if (this.userService.LoggedUser.username != null) {
			console.log("profileGuard: userService Seems connected with '"+ this.userService.LoggedUser.username +"' profile.");
			return true;
		}
		else {
			this.router.navigate(['/login']);
		}
	}

}
