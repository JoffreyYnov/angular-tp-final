import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/index';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class UserService {

	public LoggedUser = {
		username: null,
		role: 0
	};

	public serviceData = "./assets/users.json";


	constructor(private http: HttpClient) { }


	getUsers(): Observable<any> {
		return this.http.get(this.serviceData);
	}

	logOut() {
		console.log('Unlogging from "' + this.LoggedUser.username + '"...');
		this.LoggedUser.username = null;
		this.LoggedUser.role = 0;
		console.log("Unlogged.");
	}


	userTestPost() {
		var dataTest = {
			data1 : 1,
			data2: 2
		};
		console.log("UserTestPost");
		this.http.post(this.serviceData, dataTest).subscribe(
			(response) => {
				console.log(response);
			},
			(error) => {
				console.log(error);
			});
	}
}
